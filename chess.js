var topic = 'iuhgougwougwigfwieg';
var client =new Paho.MQTT.Client('tw.igs.farm',8083,'cid'+Math.random());
client.connect({
  userName:'scott',
  password:'tiger',
  useSSL:true,
  onSuccess:function(){
    client.subscribe(topic);
  }
})

client.onMessageArrived = function(d){
  let inst = JSON.parse(d.payloadString);
  updateState(inst);
  updateScreen();
  console.log(inst);
}

var pieceList = [9820,9822,9821,9819,9818,9821,9822,9820,
                 9823,9823,9823,9823,9823,9823,9823,9823,
                 0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0,
                 0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0,
                 9817,9817,9817,9817,9817,9817,9817,9817,
                 9814,9816,9815,9813,9812,9815,9816,9814
                 ];

let state = {};

for (let j=0;j<8;j++){
  let row = $('<div/>',{'class':'rw'})
    .appendTo('#board-container');
  for(let i=0;i<8;i++){
    $('<div/>',{'class':'sq',data:{index:8*j+i}})
      .appendTo(row);
  }
}

$('.sq').click(function(){
  //Are we choosing source or destination?
  if ($('.src').length === 0){
    // Nothing selected yet... hi-light the source
    $('.src').removeClass('src');
    $(this).addClass('src');
    $('#board-container').addClass('state-move');
  }else{
    // Second click - pick destination
    $('#board-container').removeClass('state-move');
    if ($('.src').data('index') === $(this).data('index')){
      //Same piece click - undo the selection
      $('.src').removeClass('src');
      return;
    }
    let inst = {action:'move',
                src:$('.src').data('index'),
                dst: $(this).data('index')};
    if (! validMove(inst))
      return;
    client.send(topic,JSON.stringify(inst));
    $('.src').removeClass('src');
  }
});

function validMove(inst){
  if (state.whoToGo === 'white'){
    return state.board[inst.src] <= 9817;
  }
  if (state.whoToGo === 'black'){
    return state.board[inst.src] > 9817;
  }
}

$('#new-game').click(function(){
  //updateState({action:'reset-board'});
  //updateScreen();
  client.send(topic,JSON.stringify({action:'reset-board'}));
});


//Update the global variable to respect some instruction
function updateState(inst){
  if (inst.action === 'reset-board'){
    state.board = pieceList.map(x=>x);
    state.whoToGo = 'white';
  }
  if (inst.action === 'move'){
    state.board[inst.dst] = state.board[inst.src];
    state.board[inst.src] = 0;
    state.whoToGo = state.whoToGo === 'white'?'black':'white';
  }
}

//Draw/redraw the screen to reflect the global state
function updateScreen(){
  for(let j=0;j<8;j++){
    for(let i=0;i<8;i++){
      let code = state.board[8*j+i];
      if (code>0)
        $($('.sq',$('.rw')[j])[i]).html('&#'+state.board[8*j+i]+';');
      else
        $($('.sq',$('.rw')[j])[i]).html('');
    }
  }
}

